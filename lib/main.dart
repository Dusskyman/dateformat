import 'package:flutter/material.dart';

import 'package:scroll_list/utils/date_formater.dart';

void main() {
  runApp(MaterialApp(home: MyApp()));
}

class MyApp extends StatelessWidget {
  DateTime dateTime = DateTime.now();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Не отформатированное время ${dateTime.toString()}',
          ),
          Text(
            'Форматированно ${DateFormater.formatTOyMd(dateTime)}',
          ),
        ],
      ),
    );
  }
}
